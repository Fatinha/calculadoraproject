package com.br.classes;

/**
 *
 * @author Fatinha de Sousa
 */

public class Calculadora {

    public int somar(Integer a, Integer b){
        
        if((a == null) || (b == null)){
            throw new OperacaoMatematicaException("Valores Nulos");
        }
        return a + b;
    }
    
    public int subtrair(Integer a, Integer b){
        
        if((a == null) || (b == null)){
            throw new OperacaoMatematicaException("Valores Nulos");
        }
        
        return a - b;
    }
    
    public int multiplicar(Integer a, Integer b){
        
        if((a == null) || (b == null)){
            throw new OperacaoMatematicaException("Valores Nulos");
        }
        
        return a * b;
    }
    
    public int dividir(Integer a, Integer b){
        
        if((a == null) || (b == null)){
            throw new OperacaoMatematicaException("Valores Nulos");
        }
        
        return a / b;
    }
}
