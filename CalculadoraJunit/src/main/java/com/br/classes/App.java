package com.br.classes;

/**
 *
 * @author Fatinha de Sousa
 */

public class App {

    public static void main(String [] args){
        
        Calculadora calculadora = new Calculadora();
        
        System.out.println("Soma: " +calculadora.somar(5, 5));
        System.out.println("Subtrair: " +calculadora.subtrair(10, 5));
        System.out.println("Multiplicação: " +calculadora.multiplicar(5, 5));
        System.out.println("Subtração: " +calculadora.dividir(15, 3));
    }
}
