package com.br.classes;

/**
 *
 * @author Fatinha de Sousa
 */

public class OperacaoMatematicaException extends RuntimeException{
    
    public OperacaoMatematicaException(){
    
    }
    
    public OperacaoMatematicaException(String message) {
        super(message);
    }
}
