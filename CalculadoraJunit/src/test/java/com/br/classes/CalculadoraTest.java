package com.br.classes;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fatinha de Sousa
 */
public class CalculadoraTest{
    
    private Calculadora calculadora;
    
    public CalculadoraTest(){
        calculadora = new Calculadora();
    }
    
    @Test
    public void testeSomaValoresPositivos(){
        assertEquals("Soma", 10, calculadora.somar(5, 5));
        assertFalse("Valores Diferentes", 10 == calculadora.somar(6, 5));
        assertTrue("Valores Diferentes", 10 == calculadora.somar(4, 6));
        assertFalse("Valores Iguais", 10 == calculadora.somar(6, 6));
    }
    
    @Test
    public void testeSomaValoresNegativos(){
        assertEquals("Soma", 10, calculadora.somar(-20, 30));
        assertFalse("Valores Diferentes", 10 == calculadora.somar(-20, -15));
        assertFalse("Valores Iguais", 10 == calculadora.somar(-10, -10));
        assertTrue("Soma", 10 == calculadora.somar(-20, 30));
    }
    
    @Test(expected = OperacaoMatematicaException.class)
    public void testeSomaValoresNulos(){
        assertEquals(0, calculadora.somar(null, 10));
        assertFalse(10 == calculadora.somar(null, 25));
        assertTrue(10 == calculadora.somar(null, 25));
    }
}
